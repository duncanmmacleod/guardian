"""
aLIGO Guardian python library
"""

# tell guardian to not write bytecode (.pyc) files.  This is meant to
# circumvent the creation of .pyc files for usercode, which tend to
# cause confusion and clutter much more than providing any system
# speedup.
import sys
sys.dont_write_bytecode = True

from ._version import __version__
from .state import GuardState, GuardStateDecorator
from .system import GuardSystem

try:
    from .manager import Node, NodeManager
except ImportError:
    pass
