from guardian import GuardState

class INIT(GuardState):
    pass

class A(GuardState):
    goto = True

class B(GuardState):
    index = 1
    request = False

class C(GuardState):
    index = 10

class D(GuardState):
    index = 2382
